# Cron

This is a base Alpine Linux image intended for extension by other Dockerfiles.
Rename the "crontab.sample" file to a username that should run the tasks, e.g.
"root" and place it in the "/var/spool/cron/crontabs" directory. Cron will
automatically pick it up and run it.

The example crontab runs all scripts in the sub-folders of "/etc/periodic" at
fixed intervals.
